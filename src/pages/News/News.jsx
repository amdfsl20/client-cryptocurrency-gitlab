import React, { useState } from 'react';
import { Select, Typography, Row, Col, Avatar, Card } from 'antd';
import moment from 'moment';

import { useGetCryptoNewsQuery } from '@/services/cryptoNewsApi';
import { useGetCryptosQuery } from '@/services/cryptoApi';
import { Loader } from '@/components/atoms';
import style from './_news.module.scss';
import demoImage from '@/images/default-image.png';

const { Text, Title } = Typography;
const { Option } = Select;

const News = ({ simplified }) => {
  const [newsCategory, setNewsCategory] = useState('Cryptocurrency');
  const { data: crytoNews, isFetching } = useGetCryptoNewsQuery({
    newsCategory: newsCategory,
    count: simplified ? 10 : 100,
  });
  const { data } = useGetCryptosQuery(100);

  if (isFetching) return <Loader />;

  return (
    <Row gutter={[24, 24]} className={style.news}>
      {!simplified && (
        <Col span={24}>
          <Select
            showSearch
            className={style.select}
            placeholder="Select a Crypyo"
            optionFilterProp="children"
            onChange={(value) => setNewsCategory(value)}
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
          >
            <Option value="Cryptocurrency">Cryptocurrency</Option>
            {data?.data?.coins.map((coin, index) => (
              <Option key={index} value={coin.name}>
                {coin.name}
              </Option>
            ))}
          </Select>
        </Col>
      )}
      {crytoNews.value.map((news, i) => (
        <Col xs={24} sm={12} lg={8} key={i}>
          <Card hoverable className={style.card}>
            <a href={news.url} target="_blank" rel="noreferrer">
              <div className={style.image}>
                <Title className={style.title} level={5}>
                  {news.name}
                </Title>
                <img
                  style={{ maxWidth: '200px', maxHeight: '200px' }}
                  src={news?.image?.thumbnail?.contentUrl || demoImage}
                  alt={news.name}
                />
              </div>
              <p className={style.text}>
                {news.description.length > 100
                  ? `${news.description.substring(0, 100)} ...`
                  : news.description}
              </p>
              <div className={style.provider}>
                <Avatar
                  src={
                    news.provider[0]?.image?.thumbnail?.contentUrl || demoImage
                  }
                  alt="news"
                />
                <Text className={style.provider_name}>
                  {news.provider[0]?.name}
                </Text>
                <Text>
                  {moment(news.datePublished).startOf('ss').fromNow()}
                </Text>
              </div>
            </a>
          </Card>
        </Col>
      ))}
    </Row>
  );
};

export default News;
