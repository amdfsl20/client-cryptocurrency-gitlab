import { MenuOutlined } from '@ant-design/icons';
import React from 'react';
import PropTypes from 'prop-types';
import style from './_menuButton.module.scss';
import { Button } from '@/components/atoms';

const MenuButton = ({ onClick }) => {
  return (
    <>
      <Button
        className={style.menuButton}
        color="blue"
        content={<MenuOutlined className={style.content} />}
        onClick={onClick}
      />
    </>
  );
};

export default MenuButton;

MenuButton.defaultProps = {
  onClick: undefined,
};

MenuButton.propTypes = {
  onClick: PropTypes.func,
};
