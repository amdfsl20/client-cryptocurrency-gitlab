import React from 'react';
import { Button as AntButton } from 'antd';
import PropTypes from 'prop-types';
import style from './_button.module.scss';

export default function Button({ content, className, onClick, color }) {
  return (
    <AntButton
      className={`${style.btn} ${color && style[color]} ${className}`}
      onClick={onClick}
    >
      {content}
    </AntButton>
  );
}

Button.defaultProps = {
  content: 'Button',
  className: '',
  onClick: undefined,
  color: 'green',
};

Button.propTypes = {
  content: PropTypes.element,
  className: PropTypes.string,
  onClick: PropTypes.func,
  color: PropTypes.oneOf(['green', 'blue']),
};
