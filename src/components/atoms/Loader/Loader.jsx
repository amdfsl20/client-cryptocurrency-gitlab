import React from 'react';
import { Spin } from 'antd';
import PropTypes from 'prop-types';
import style from './_loader.module.scss';

const Loader = () => {
  return <Spin className={style.loader} />;
};

export default Loader;

Loader.defaultProps = {
  className: '',
};

Loader.propTypes = {
  className: PropTypes.string,
};
