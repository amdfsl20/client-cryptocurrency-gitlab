import React from 'react';
import { Space, Typography } from 'antd';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import style from './_footer.module.scss';

const Footer = () => {
  return (
    <div className={style.footer}>
      <Typography.Title level={5}>
        Copyright © 2022 by
        <Link to="/"> Ahmad Faisal</Link>
      </Typography.Title>
      <Space>
        <Link to="/">Home</Link>
        <Link to="/cryptocurrencies">Cryptocurrencies</Link>
        <Link to="/news">News</Link>
      </Space>
    </div>
  );
};

export default Footer;

Footer.defaultProps = {
  onClick: undefined,
};

Footer.propTypes = {
  onClick: PropTypes.func,
};
